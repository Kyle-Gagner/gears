import gear
from math import pi
import cairo

size = 800
surface = cairo.ImageSurface(cairo.Format.RGB24, size, size)
context = cairo.Context(surface)

subdivisions = 20

a = pi * 25 / 180
g1 = gear.Spur(1, 12, a, 1.0, 1.25)
g2 = gear.Internal(1, 48, a, 1.0, 1.25)
g3 = gear.Spur(1, 18, a, 1.0, 1.25)
g4 = gear.Spur(1, 60, a, 1.0, 1.25)
g5 = gear.Rack(1, a, 1.0, 1.25)

context.scale(size, size)
context.translate(0.5, 0.5)

scale = 0.9 / g4.outside_diameter
context.scale(scale, scale)

context.set_source_rgb(0.1, 0.2, 0.4)
context.paint()

context.set_source_rgb(0.9, 0.9, 0.9)
context.set_line_width(0.1)
context.set_line_join(cairo.LINE_JOIN_ROUND)

g1.draft(context, subdivisions)
context.stroke()

g2.draft(context, subdivisions)
context.stroke()

for n in range(3):
    context.save()
    context.rotate(2 * pi * n / 3)
    context.translate((g1.pitch_circle_diameter + g3.pitch_circle_diameter) / 2, 0)
    context.rotate(pi / g3.number_of_teeth)
    g3.draft(context, subdivisions)
    context.stroke()
    context.restore()

g4.draft(context, subdivisions)
context.stroke()

context.rotate(pi)
context.translate(-g5.circular_pitch * 12.5, -g4.pitch_circle_diameter / 2)
g5.draft(context, 25)
context.stroke()

surface.write_to_png('test.png')
