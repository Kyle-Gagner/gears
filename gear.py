import cairo
from math import sin, cos, tan, acos, pi, ceil
import numpy as np

class Gear:
    def __init__(self, modulus, pressure_angle, addendum_ratio, dedendum_ratio):
        self._modulus = modulus
        self._pressure_angle = pressure_angle
        self._addendum_ratio = addendum_ratio
        self._dedendum_ratio = dedendum_ratio
    
    def minimum_teeth(pressure_angle):
        """Computes the minimum number of teeth for a gear with a given pressure angle to avoid undercut."""
        return int(ceil(2 / (sin(pressure_angle) ** 2)))
    
    @property
    def diametral_pitch(self):
        """Gets the diametral pitch, the ratio of the number of teeth to the pitch circle diameter."""
        return 1 / self.modulus
    
    @property
    def circular_pitch(self):
        """Gets the circular pitch, the ratio of the pitch circle circumference to the number of teeth.
        Alternately, the spacing between teeth on a rack."""
        return pi * self.modulus
    
    @property
    def modulus(self):
        """Gets the modulus, the ratio of the pitch circle diameter to the number of teeth."""
        return self._modulus
    
    @property
    def pressure_angle(self):
        """Gets the pressure angle of the gear's teeth."""
        return self._pressure_angle
    
    @property
    def addendum(self):
        """Gets the addendum."""
        return self._addendum_ratio * self.modulus
    
    @property
    def dedendum(self):
        """Gets the dedendum."""
        return self._dedendum_ratio * self.modulus

class Rack(Gear):
    def tooth_thickness_at(self, depth):
        """Gets the tooth thickness at a given depth."""
        return - 2 * tan(self.pressure_angle) * depth + self.circular_pitch / 2
    
    def draft(self, context, number_of_teeth):
        """Drafts the gear's profile using line_to on a cairo.Context-like context."""
        top_land = self.tooth_thickness_at(self.addendum)
        bottom_land = self.tooth_thickness_at(-self.dedendum)
        context.line_to(0, self.addendum)
        for n in range(number_of_teeth):
            x = n * self.circular_pitch
            context.line_to(x + top_land / 2, self.addendum)
            context.line_to(x + bottom_land / 2, -self.dedendum)
            context.line_to(x + self.circular_pitch - bottom_land / 2, -self.dedendum)
            context.line_to(x + self.circular_pitch - top_land / 2, self.addendum)
        context.line_to(number_of_teeth * self.circular_pitch, self.addendum)

class Involute(Gear):
    def __init__(self, modulus, number_of_teeth, pressure_angle, addendum_ratio, dedendum_ratio):
        Gear.__init__(self, modulus, pressure_angle, addendum_ratio, dedendum_ratio)
        self._number_of_teeth = number_of_teeth
    
    @staticmethod
    def __involute(angle):
        return tan(angle) - angle
    
    @property
    def number_of_teeth(self):
        """Gets the number of teeth."""
        return self._number_of_teeth
    
    def tooth_thickness_at(self, diameter):
        """Gets the tooth thickness at a given diameter."""
        if diameter < self.base_circle_diameter:
            return self.tooth_thickness_at(self.base_circle_diameter) * diameter / self.base_circle_diameter
        return diameter * (pi / (2 * self.number_of_teeth) + self.__involute(self.pressure_angle) - \
            self.__involute(acos(self.pitch_circle_diameter * cos(self.pressure_angle) / diameter)))
    
    @property
    def base_pitch(self):
        """Gets the circular pitch of the teeth as measured at the base circle, rather than the pitch circle."""
        return self.circular_pitch * cos(self.pressure_angle)

    @property
    def pitch_circle_diameter(self):
        """Gets the pitch circle diameter."""
        return self.number_of_teeth * self.modulus
    
    @property
    def base_circle_diameter(self):
        """Gets the base circle diameter according to the pitch circle diameter and the pressure angle."""
        return self.pitch_circle_diameter * cos(self.pressure_angle)
    
    def draft(self, context, subdivisions):
        """Drafts the gear's profile using line_to, arc, and close_path functions on a cairo.Context-like context."""
        root_angle = self.tooth_thickness_at(self.root_diameter) / self.root_diameter
        tip_angle = self.tooth_thickness_at(self.__tip_diameter) / self.__tip_diameter
        pitch_angle = 2 * pi / self.number_of_teeth
        for i in range(self.number_of_teeth):
            theta = i * pitch_angle
            context.arc(0, 0, self.__tip_diameter / 2, theta - tip_angle, theta + tip_angle)
            for j in range(subdivisions if self.base_circle_diameter > self.root_diameter else subdivisions - 1):
                sub_diameter = self.__tip_diameter + \
                    (max(self.root_diameter, self.base_circle_diameter) - self.__tip_diameter) * \
                        (j + 1) / subdivisions
                sub_angle = self.tooth_thickness_at(sub_diameter) / sub_diameter
                context.line_to(sub_diameter * cos(theta + sub_angle) / 2, \
                    sub_diameter * sin(theta + sub_angle) / 2)
            context.arc(0, 0, self.root_diameter / 2, theta + root_angle, theta + pitch_angle - root_angle)
            for j in range(0 if self.base_circle_diameter > self.root_diameter else 1, subdivisions):
                sub_diameter = self.__tip_diameter + \
                    (max(self.root_diameter, self.base_circle_diameter) - self.__tip_diameter) * \
                        (subdivisions - j) / subdivisions
                sub_angle = self.tooth_thickness_at(sub_diameter) / sub_diameter
                context.line_to(sub_diameter * cos(theta + pitch_angle - sub_angle) / 2, \
                    sub_diameter * sin(theta + pitch_angle - sub_angle) / 2)
        context.close_path()

class Spur(Involute):
    @property
    def _Involute__tip_diameter(self):
        return self.outside_diameter
    
    @property
    def outside_diameter(self):
        """Gets the outside diameter, the pitch circle diameter plus twice the addendum."""
        return self.pitch_circle_diameter + 2 * self.addendum
    
    @property
    def root_diameter(self):
        """Gets the root diameter, the pitch circle diameter less twice the dedendum."""
        return self.pitch_circle_diameter - 2 * self.dedendum

class Internal(Involute):
    def tooth_thickness_at(self, diameter):
        """Gets the tooth thickness at a given diameter."""
        return self.circular_pitch * diameter / self.pitch_circle_diameter - Involute.tooth_thickness_at(self, diameter)

    @property
    def _Involute__tip_diameter(self):
        return self.inside_diameter

    @property
    def inside_diameter(self):
        """Gets the inside diameter, the pitch circle diameter less twice the addendum."""
        return self.pitch_circle_diameter - 2 * self.addendum
    
    @property
    def root_diameter(self):
        """Gets the root diameter, the pitch circle diameter plus twice the dedendum."""
        return self.pitch_circle_diameter + 2 * self.dedendum
